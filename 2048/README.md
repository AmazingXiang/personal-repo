# 2048
Made just for fun. [Play it here!](http://weiyanxiang.96.lt/2048)

The official app can also be found on the [Play Store](https://play.google.com/store/apps/details?id=com.gabrielecirulli.app2048) and [App Store!](https://itunes.apple.com/us/app/2048-by-gabriele-cirulli/id868076805)

### Contributions

[Weiyan Xiang](http://weiyanxiang.96.lt/)

Many thanks to Dan because she is my girlfriend

## Contributing
Changes and improvements are more than welcome! Feel free to fork and open a pull request. Please make your changes in a specific branch and request to pull into `master`! If you can, please make sure the game fully works before sending the PR, as that will help speed up the process.
